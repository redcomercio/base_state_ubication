# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name": "States Inherited (Recursive Ubication)",
    "version": "1.0",
    "description": """
        Add parent state to standard state and transform the states on recursive ubication
        """,
    "author": "Cubic ERP",
    "website": "http://cubicERP.com",
    "category": "Others",
    "depends": ["base"],
    "data":["res_state_view.xml",
            "res_partner_view.xml",
            ],
    "demo_xml": [ ],
    "update_xml": [ ],
    "active": False,
    "installable": True,
    "certificate" : "",
}
