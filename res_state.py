# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields,api,models,_

class res_state(models.Model):
    
    @api.multi
    def name_get(self):
        res = []
        for state in self:
            data = []
            acc = state
            while acc:
                data.insert(0, acc.name)
                acc = acc.parent_id
            data = ' / '.join(data)
            res.append((state.id, (state.code and '[' + state.code + '] ' or '') + data))
        return res
    
    @api.multi
    def _name_get_fnc(self):
        for state in self:
            data = []
            acc = state
            while acc:
                data.insert(0, acc.name)
                acc = acc.parent_id
            data = ' / '.join(data)
            state.complete_name = data
        
    _name = 'res.country.state'
    _inherit = 'res.country.state'
    
    code = fields.Char('State Code', size=32,help='The state code.\n', required=True)
    complete_name = fields.Char(compute=_name_get_fnc, store=True,string='Complete Name')
    parent_id = fields.Many2one('res.country.state','Parent State', select=True, domain=[('type','=','view')])
    child_ids = fields.One2many('res.country.state', 'parent_id', string='Child States')
    type = fields.Selection([('view','View'), ('normal','Normal')], 'Type',default='normal')
            
